import { useEffect, useState} from 'react';
import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import HatsForm from './HatsForm';
import HatList from './HatsList';



function App(props) {
  const [hats, setHats] = useState([])

  const getHats = async () => {
    const hatUrl = 'http://localhost:8090/api/hats';
    const hatResponse = await fetch(hatUrl)
    //console.log(hatResponse)

    if (hatResponse.ok) {
      const hatsData = await hatResponse.json();
      const hats = hatsData.hats
      setHats(hats)
    }
  }
  useEffect(() => {getHats()}, [])


  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
        <Route index element={<MainPage />} />
          <Route path="hats">
          <Route index element={<HatList hats={hats} getHats={getHats} />} />
          <Route path="new" element={<HatsForm getHats={getHats} />} />
          </Route>
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
