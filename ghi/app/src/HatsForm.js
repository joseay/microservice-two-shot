import React, { useEffect, useState} from 'react';

function HatsForm(props) {

    const[locations, setLocations] = useState([]);
    const[fabric, setFabric] = useState('');
    const[styleName, setStyleName] = useState('');
    const[color, setColor] = useState('');
    const[pictureUrl, setPictureUrl] = useState('');
    const[location, setLocation] = useState('');
    
    const handleFabricInputChange = (event) => {
        setFabric(event.target.value);
    };

    const handleStyleNameInputChange = (event) => {
        setStyleName(event.target.value);
    };

    const handleColorInputChange = (event) => {
        setColor(event.target.value);
    };

    const handlePictureUrlInputChange = (event) => {
        setPictureUrl(event.target.value);
    };

    const handleLocationInputChange = (event) => {
        setLocation(event.target.value);
    };

    const handleSubmit = async (event) => {
        event.preventDefault();

        const data = {};
        data.fabric = fabric;
        data.style_name = styleName;
        data.color = color;
        data.picture_url = pictureUrl;
        data.location = location;
        //console.log(data)
        const hatUrl = 'http://localhost:8090/api/hats/';
        //console.log(hatUrl);
        const fetchConfig = {
        method: 'POST',
        body: JSON.stringify(data),
        headers: {'Content-Type': 'application/json'},
        }
        console.log(fetchConfig)

        const response = await fetch(hatUrl, fetchConfig);
        console.log(response)
            if(response.ok){
                const newHat = await response.json();
                console.log('hat status:', newHat)
                setFabric('');
                setStyleName('');
                setColor('');
                setPictureUrl('');
                setLocation('');
                props.getHats()
        console.log(newHat)
        }
    }

    const fetchData = async () => {
        const url ='http://localhost:8100/api/locations'
        const response = await fetch (url);
        if (response.ok) {
            const data = await response.json()
            setLocations(data.locations)
            //console.log(data)
        }
    };

    useEffect (() => {
        fetchData ();
    }, []);

    return (
    <div className="row">
        <div className="offset-3 col-6">
            <div className="shadow p-4 mt-4">
                <h1>Create a new hat</h1>
                <form onSubmit ={handleSubmit}id="create-hat-form">
                    <div className="form-floating mb-3">
                        <input onChange={handleStyleNameInputChange}placeholder="style_name" required type="text" name="style_name" id="style_name" className="form-control" value={styleName}/>
                        <label htmlFor="style_name">Style Name</label>
                    </div>
                    <div className="form-floating mb-3">
                        <input onChange={handleFabricInputChange}placeholder="fabric" required type="text" name="fabric" id="fabric" className="form-control" value={fabric}/>
                        <label htmlFor="fabric">Fabric</label>
                    </div>
                    <div className="form-floating mb-3">
                        <input onChange={handleColorInputChange}placeholder="color" required type="text" name="color" id="color" className="form-control" value={color}/>
                        <label htmlFor="color">Color</label>
                    </div>
                    <div className="form-floating mb-3">
                        <input onChange={handlePictureUrlInputChange}placeholder="picture_url" required type="text" name="picture_url" id="picture_url" className="form-control" value={pictureUrl}/>
                        <label htmlFor="picture_url">Picture URL</label>
                    </div>
                    <div className="mb-3">
                        <select onChange={handleLocationInputChange}required name="location" id="location" className="form-select" value={location}>
                        <option value="">Choose a Location</option>
                        {locations.map(location =>{
                            return (
                                <option key={location.href} value={location.href}>
                                    {location.closet_name}
                                </option>
                                )
                            })}
                        </select>
                    </div>
                <button className="btn btn-primary">Create</button>
            </form>
        </div>
    </div>
</div>
);

}
export default HatsForm

