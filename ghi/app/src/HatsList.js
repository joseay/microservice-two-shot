

function HatList (props) {
    async function handleDelete (id) {
    const hatUrl = `http://localhost:8090/api/hats/${id}/`
    const fetchConfig = {method: 'DELETE'};
    const response = await fetch(hatUrl, fetchConfig);
    if (response.ok) {
        const deleted = await response.json();
        props.getHats()
    }
}
    return (
        <table className="table table-striped table-hover">
            <thead>
                <tr>
                    <th>Fabric</th>
                    <th>Style Name</th>
                    <th>Color</th>
                    <th>Picture</th>
                    <th>location</th>
                </tr>
            </thead>
            <tbody>
                {props.hats.map(hat =>{
                return (
                        <tr key = {hat.id}>
                            <td>{hat.fabric}</td>
                            <td>{hat.style_name}</td>
                            <td>{hat.color}</td>
                            <td><img src={hat.url_picture} width="100" alt ="Hat"/></td>
                            <td>{hat.location.closet_name}</td>
                            <td><button onClick={() => handleDelete(hat.id)} type="button" className="btn btn-danger">Delete</button></td>
                        </tr>
                    );
                })}
            </tbody>
        </table>
    )
}
export default HatList
