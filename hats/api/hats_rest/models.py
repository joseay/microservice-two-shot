from django.db import models
from django.urls import reverse
# Create your models here.

class LocationVO(models.Model):
    closet_name = models.CharField(max_length=100)
    section_number = models.PositiveSmallIntegerField(default=0, blank=True , null=True)
    shelf_number = models.PositiveSmallIntegerField(default=0, blank=True , null=True)
    import_href = models.CharField(max_length=100, unique=True, null=True)

class Hats(models.Model):
    fabric = models.CharField(max_length=50)
    style_name = models.CharField(max_length=100);
    color = models.CharField(max_length=50);
    picture_url = models.URLField(null=True, blank=True);
    location = models.ForeignKey(
        LocationVO, 
        related_name='hats',
        on_delete=models.CASCADE,
        null=True,
        )
    def __str__(self):
        return self.style_name
