from django.shortcuts import render
from .models import LocationVO, Hats
from common.json import ModelEncoder
import json
from django.views.decorators.http import require_http_methods
from django.http import JsonResponse
# Create your views here.

class locationVOdetailEncoder(ModelEncoder):
    model = LocationVO
    properties = [
        'closet_name',
        'section_number',
        'shelf_number',
        'import_href',
    ]

class HatsListEncoder(ModelEncoder):
    model = Hats
    properties = [
        'id',
        'fabric',
        'style_name',
        'color',
        'picture_url',
        'location',
    ]
    encoders = {
        'location': locationVOdetailEncoder(),
        }

class HatsDetailEncoder(ModelEncoder):
    model = Hats
    properties = [
        "id",
        "fabric",
        "style_name",
        "color",
        "picture_url",
        "location",
        ]
    encoders = {
        "location": locationVOdetailEncoder(),
    }

@require_http_methods(["GET", "POST"])
def api_list_hats(request, location_vo_id=None):
    # get the list of hat objects
    if request.method == "GET":
        if location_vo_id is not None:
            hats = Hats.objects.filter(location=location_vo_id)
        else:
            hats = Hats.objects.all()
        return JsonResponse(
            {"hats": hats},
            encoder=HatsListEncoder,
            )
    # create a hat object
    else:
        content = json.loads(request.body)
        try:
            location_href = content['location']
            location = LocationVO.objects.get(import_href=location_href)
            content['location'] = location

        except LocationVO.DoesNotExist:
            return JsonResponse(
                {'message': 'Location does not exist'},
                status=404
                )
            
        hats = Hats.objects.create(**content)
        return JsonResponse(
            hats,
            encoder=HatsDetailEncoder,
            safe=False,
        )
    
@require_http_methods(["GET", "PUT", "DELETE"])
def api_show_hats(request, pk):
    # get hat information
    if request.method == 'GET':
        hats = Hats.objects.get(id=pk)
        print('THISSSS ISSSS A TESSSST',hats)
        return JsonResponse(
            hats,
            encoder=HatsDetailEncoder,
            safe=False,
            )
    # delete hat information
    elif request.method == 'DELETE':
        count, _ = Hats.objects.filter(id=pk).delete()
        return JsonResponse(
            {'deleted': count > 0}
            )
    # update hat information
    else:
        content = json.loads(request.body)
        try:
            if "location" in content:
                location = LocationVO.objects.get(import_href=content["location"])
                content['location'] = location
        except LocationVO.DoesNotExist:
            return JsonResponse(
                {"message": "Location does not exist"},
                status = 400
            )
        Hats.objects.filter(id=pk).update(**content)
        hats = Hats.objects.get(id=pk)
        return JsonResponse(
            hats, 
            encoder=HatsDetailEncoder, 
            safe=False)
